const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const db = require('./db.js');

db.sequelize.sync().then(() => { });
app.use(bodyParser.text());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', express.static(__dirname + '/public/html'));
app.use('/', express.static(__dirname + '/public/css'));
app.use('/', express.static(__dirname + '/public/images'));
app.use('/', express.static(__dirname + '/public/js'));

app.get('/vjezbe', (req, res) => {
  var nizPromisea = [];
  let objekat, zadaci = [];
  nizPromisea.push(db.Vjezba.findAll({ where: {} }).then(function (vjezbe) {
    vjezbe.forEach(vjezba => {
      nizPromisea.push(db.Zadatak.findAll({ where: { VjezbaId: vjezba.id } }).then(function (zadaci1) {
        let pom = vjezba.naziv.split(" ");
        poz = pom[1] - 1;
        zadaci[poz] = zadaci1.length;
        objekat = {
          brojVjezbi: vjezbe.length,
          brojZadataka: zadaci
        };

        if (objekat.brojVjezbi == Object.values(zadaci).length) {
          Promise.all(nizPromisea).then(function () {

            let greska = "Pogrešan parametar ";
            let ispravan = true;
            let broj = objekat.brojVjezbi;

            if (broj != zadaci.length || broj < 1 || broj > 15) {
              greska += "brojVjezbi";
              ispravan = false;
            }

            for (let i = 0; i < zadaci.length; i++) {
              if (zadaci[i] > 10 || zadaci[i] < 0) {
                if (ispravan == true) {
                  greska += "z" + i;
                  ispravan = false;
                } else {
                  greska += ",z";
                }
              }
            }
            let objekat2 = {
              status: "error",
              data: greska
            };
            if (ispravan == false) {
              res.writeHead(400, { 'Content-Type': 'application/json' });
              res.end(JSON.stringify(objekat2));
            } else {
              res.writeHead(200, { 'Content-Type': 'application/json' });
              res.end(JSON.stringify(objekat));
            }
          }).catch(function (error) {
            console.log(error);
          });
        }
      }).catch(function (error) {
        console.log(error);
      }));
    });
  }));
});
app.post('/vjezbe', (req, res) => {
  let tijelo = req.body;
  let broj = tijelo['brojVjezbi'];
  let zadaci = tijelo['brojZadataka'];
  let greska = "Pogrešan parametar ";
  let ispravan = true;
  if (broj != zadaci.length || broj < 1 || broj > 15) {
    greska += "brojVjezbi";
    ispravan = false;
  }
  for (let i = 0; i < zadaci.length; i++) {
    if (zadaci[i] > 10 || zadaci[i] < 0) {
      if (ispravan == true) {
        greska += "z" + i;
        ispravan = false;
      } else {
        greska += ",z" + i;
      }

    }
  }
  let objekat = {
    status: "error",
    data: greska
  };
  if (ispravan == false) {
    res.writeHead(400, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify(objekat));
  } else {
    objekat = {
      brojVjezbi: zadaci.length,
      brojZadataka: zadaci
    }
    let vjezbaPromise = [];
    db.Zadatak.destroy({ cascade: true, where: {} }).then(() => {
      db.Vjezba.destroy({ cascade: true, where: {} })
        .then(function () {

          for (let i = 0; i < broj; i++) {
            let br = i + 1;

            vjezbaPromise.push(db.Vjezba.create({ naziv: "Vjezba " + br }).then(function (vjezba) {
              for (let j = 0; j < zadaci[i]; j++) {
                let br1 = j + 1;
                vjezbaPromise.push(db.Zadatak.create({ naziv: "Zadatak " + br1 }).then(function (zadatak) {
                  vjezbaPromise.push(db.Zadatak.update({ VjezbaId: vjezba.id }, { where: { id: zadatak.id } }).then(() => {

                  }));
                }));

              }

            }));
          }
        }).catch(function (error) {
          console.log(error);
        });
      Promise.all(vjezbaPromise).then(() => {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify(objekat));
      });
    });
  }
});
app.post('/student', (req, res) => {
  let tijelo = req.body;
  let ime = tijelo['ime'];
  let prezime = tijelo['prezime'];
  let index = tijelo['index'];
  let grupa = tijelo['grupa'];
  db.Student.findOne({ where: { index: index } }).then(function (student) {

    if (student != null) {
      let objekat = {
        status: "Student sa indexom {" + index + "} već postoji!"
      }
      res.writeHead(400, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify(objekat));
    } else {
      db.Grupa.findOrCreate({ where: { naziv: grupa } }).then(function (gr) {
        db.Student.create({ ime: ime, prezime: prezime, index: index, grupa: grupa }).then(function (k) {
          db.Grupa.findOne({ where: { naziv: grupa } }).then(function (grup) {
            db.Student.update({ GrupaId: grup.id }, { where: { index: index } }).then(function () {
              let objekat = {
                status: "Kreiran student!"
              }
              res.writeHead(200, { 'Content-Type': 'application/json' });
              res.end(JSON.stringify(objekat));
            }).catch(function (error) { console.log(error) });
          }).catch(function (error) { console.log(error) });
        }).catch(function (error) { console.log(error) });
      }).catch(function (error) { console.log(error) });
    }
  }).catch(function (error) { console.log(error) });

});
app.put('/student/:index', (req, res) => {
  let index = req.params.index;
  let tijelo = req.body;

  let grupa = tijelo['grupa'];
  db.Student.findOne({ where: { index: index } }).then(function (student) {
    if (student != null) {
      db.Grupa.findOrCreate({ where: { naziv: grupa } }).then(function (gr) {
        db.Grupa.findOne({ where: { naziv: grupa } }).then(function (grup) {
          db.Student.update({ grupa: grupa, GrupaId: grup.id }, { where: { index: index } }).then(function () {
            let objekat = {
              status: "Promjenjena grupa studentu {" + index + "}"
            }
            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify(objekat));
          }).catch(function (error) { console.log(error) });
        }).catch(function (error) { console.log(error) });
      }).catch(function (error) { console.log(error) });

    } else {
      let objekat = {
        status: "Student sa indexom {" + index + "} ne postoji!"
      }
      res.writeHead(400, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify(objekat));
    }
  }).catch(function (error) { console.log(error) });

});
app.post('/batch/student', (req, res) => {
  let tijelo = req.body;
  var parametri
  if (tijelo.includes('\r')) {
    parametri = tijelo.split('\r\n');
  } else {
    parametri = tijelo.split('\n');
  }
  let broj = parametri.length;
  var studenti = [];
  var postojeci = [];
  var zaDodati = [];
  var nizPromisea = [];
  for (let i = 0; i < broj; i++) {
    let parametri2 = parametri[i].split(",");
    student = {
      ime: parametri2[0],
      prezime: parametri2[1],
      index: parametri2[2],
      grupa: parametri2[3]
    }
    studenti.push(student);
  };

  for (let i = 0; i < studenti.length; i++) {
    for (let j = i + 1; j < studenti.length; j++) {
      if (studenti[i].index == studenti[j].index) {
        postojeci[j] = studenti[j].index;
      }
    }
  }

  for (let i = 0; i < studenti.length; i++) {
    nizPromisea.push(db.Student.findOne({ where: { index: studenti[i].index } }).then(function (student) {
      if (student != null) { postojeci[i] = student.index; } else if (postojeci[i] == null) {
        zaDodati[i] = studenti[i];
      }
    }).catch(function (error) { console.log(error) }));
  };

  Promise.all(nizPromisea).then(function () {

    var nizPromisea2 = [];
    zaDodati.forEach(element => {
      nizPromisea2.push(db.Grupa.findOrCreate({ where: { naziv: element.grupa } }).then(() => {
        nizPromisea2.push(db.Student.create({ ime: element.ime, prezime: element.prezime, index: element.index, grupa: element.grupa }).then(() => {
          nizPromisea2.push(db.Grupa.findOne({ where: { naziv: element.grupa } }).then(function (grup) {
            nizPromisea2.push(db.Student.update({ GrupaId: grup.id }, { where: { index: element.index } }).then(() => {
            }).catch(function (error) { console.log(error) }));
          }).catch(function (error) { console.log(error) }));
        }).catch(function (error) { console.log(error) }));
      }).catch(function (error) { console.log(error) }));
    });
    Promise.all(nizPromisea2).then(function () {
      let str = "Dodano {" + Object.values(zaDodati).length + "} studenata";
      if (Object.values(postojeci).length != 0) {
        str += ", a studenti {";
        postojeci.forEach(element => {
          str += element + ",";
        });
        str = str.substring(0, str.length - 1);
        str += "} već postoje!";

      } else {
        str += "!";
      }
      objekat = { status: str };
      res.writeHead(200, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify(objekat));
    });
  });
});
app.listen(3000);