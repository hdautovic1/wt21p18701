const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2118701", "root", "password",
  {
    host: "127.0.0.1",
    dialect: "mysql",
    logging: false
  });
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//import modela
db.Student = require(__dirname + '/models/student.js')(sequelize);
db.Vjezba = require(__dirname + '/models/vjezba.js')(sequelize);
db.Zadatak = require(__dirname + '/models/zadatak.js')(sequelize);
db.Grupa = require(__dirname + '/models/grupa.js')(sequelize);


//relacije
// Veza 1-n vise knjiga se moze nalaziti u biblioteci
db.Grupa.hasMany(db.Student, { as: 'studentiGrupe' });
db.Vjezba.hasMany(db.Zadatak, { as: 'zadaciVjezbe' });

module.exports = db;