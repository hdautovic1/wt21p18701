let assert = chai.assert;
describe('TestoviParser', function() {
 describe('porediRezultate()', function() {
   it('Identicni testovi', function() {
    var rez1="{\n"+
    "\"stats\": {\n" +
    "\"suites\": 2,\n"+
    "\"tests\": 3,\n"+
    "\"passes\": 3,\n"+
    "\"pending\": 0,\n"+
    "\"failures\": 0,\n"+
    "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
    "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
    "\"duration\": 9\n"+
  "},\n"+

  "\"tests\": [\n"+
    "{\n"+
        "\"title\": \"Test1\",\n"+
        "\"fullTitle\": \"Test1-FT\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 1,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
      "},\n"+

      "{\n"+
        "\"title\": \"Test2\",\n"+
        "\"fullTitle\": \"Test2-FT\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
      "},\n"+
              
    "{\n"+
        "\"title\": \"Test3\",\n"+
        "\"fullTitle\": \"ATest3-FT\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
      "}\n"+
    
   "],\n"+
    
   "\"pending\": [],\n"+
   "\"failures\": [],\n"+
   "\"passes\": [\n"+
   "{\n"+
   "\"title\": \"Test1\",\n"+
   "\"fullTitle\": \"Test1-FT\",\n"+
   "\"file\": null,\n"+
   "\"duration\": 1,\n"+
   "\"currentRetry\": 0,\n"+
   "\"speed\": \"fast\",\n"+
   "\"err\": {}\n"+
   "},\n"+
   
   "{\n"+
   "\"title\": \"Test2\",\n"+
   "\"fullTitle\": \"Test2-FT\",\n"+
   "\"file\": null,\n"+
   "\"duration\": 0,\n"+
   "\"currentRetry\": 0,\n"+
   "\"speed\": \"fast\",\n"+
   "\"err\": {}\n"+
  "},\n"+

  "{\n"+
  "\"title\": \"Test3\",\n"+
  "\"fullTitle\": \"ATest3-FT\",\n"+
  "\"file\": null,\n"+
  "\"duration\": 0,\n"+
  "\"currentRetry\": 0,\n"+
  "\"speed\": \"fast\",\n"+
  "\"err\": {}\n"+
 "}\n"+

"]\n"+
"}\n";
var rez2= "{\n"+
"\"stats\": {\n" +
"\"suites\": 2,\n"+
"\"tests\": 3,\n"+
"\"passes\": 1,\n"+
"\"pending\": 0,\n"+
"\"failures\": 2,\n"+
"\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
"\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
"\"duration\": 9\n"+
"},\n"+

"\"tests\": [\n"+
"{\n"+
    "\"title\": \"Test1\",\n"+
    "\"fullTitle\": \"Test1-FT\",\n"+
    "\"file\": null,\n"+
    "\"duration\": 1,\n"+
    "\"currentRetry\": 0,\n"+
    "\"speed\": \"fast\",\n"+
    "\"err\": {}\n"+
  "},\n"+

  "{\n"+
  "\"title\": \"Test2\",\n"+
  "\"fullTitle\": \"Test2-FT\",\n"+
  "\"file\": null,\n"+
  "\"duration\": 1,\n"+
  "\"currentRetry\": 0,\n"+
  "\"speed\": \"fast\",\n"+
  "\"err\": {}\n"+
"},\n"+
          
"{\n"+
    "\"title\": \"Test3\",\n"+
    "\"fullTitle\": \"ATest3-FT\",\n"+
    "\"file\": null,\n"+
    "\"duration\": 0,\n"+
    "\"currentRetry\": 0,\n"+
    "\"speed\": \"fast\",\n"+
    "\"err\": {}\n"+
  "}\n"+

"],\n"+

"\"pending\": [],\n"+
"\"failures\": [\n"+
"{\n"+
"\"title\": \"Test1\",\n"+
"\"fullTitle\": \"Test1-FT\",\n"+
"\"file\": null,\n"+
"\"duration\": 1,\n"+
"\"currentRetry\": 0,\n"+
"\"speed\": \"fast\",\n"+
"\"err\": {}\n"+
"},\n"+

"{\n"+
"\"title\": \"Test3\",\n"+
"\"fullTitle\": \"ATest3-FT\",\n"+
"\"file\": null,\n"+
"\"duration\": 0,\n"+
"\"currentRetry\": 0,\n"+
"\"speed\": \"fast\",\n"+
"\"err\": {}\n"+
"}\n"+

"],\n"+

"\"passes\": [\n"+

"{\n"+
"\"title\": \"Test2\",\n"+
"\"fullTitle\": \"Test2-FT\",\n"+
"\"file\": null,\n"+
"\"duration\": 0,\n"+
"\"currentRetry\": 0,\n"+
"\"speed\": \"fast\",\n"+
"\"err\": {}\n"+
"}\n"+
"]\n"+
"}\n";
     const test=TestoviParser.porediRezultate(rez1,rez2);

     assert.equal(JSON.stringify(test),"{\"promjena\":\"33.3%\",\"greske\":[\"ATest3-FT\",\"Test1-FT\"]}");
   });
   it('Razliciti testovi', function() {
    var rez1="{\n"+
    "\"stats\": {\n" +
    "\"suites\": 2,\n"+
    "\"tests\": 3,\n"+
    "\"passes\": 2,\n"+
    "\"pending\": 0,\n"+
    "\"failures\": 1,\n"+
    "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
    "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
    "\"duration\": 9\n"+
  "},\n"+

  "\"tests\": [\n"+
    "{\n"+
        "\"title\": \"Test1\",\n"+
        "\"fullTitle\": \"Test1-FT\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 1,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
      "},\n"+

      "{\n"+
      "\"title\": \"Test4\",\n"+
      "\"fullTitle\": \"Test4-FT\",\n"+
      "\"file\": null,\n"+
      "\"duration\": 0,\n"+
      "\"currentRetry\": 0,\n"+
      "\"speed\": \"fast\",\n"+
      "\"err\": {}\n"+
    "},\n"+
   
              
    "{\n"+
        "\"title\": \"Test3\",\n"+
        "\"fullTitle\": \"Test3-FT\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
      "}\n"+
    
   "],\n"+
    
   "\"pending\": [],\n"+
   "\"failures\": [\n"+
   "{\n"+
   "\"title\": \"Test4\",\n"+
   "\"fullTitle\": \"Test4-FT\",\n"+
   "\"file\": null,\n"+
   "\"duration\": 0,\n"+
   "\"currentRetry\": 0,\n"+
   "\"speed\": \"fast\",\n"+
   "\"err\": {}\n"+
 "}\n"+
   "],\n"+
   "\"passes\": [\n"+
   "{\n"+
   "\"title\": \"Test1\",\n"+
   "\"fullTitle\": \"Test1-FT\",\n"+
   "\"file\": null,\n"+
   "\"duration\": 1,\n"+
   "\"currentRetry\": 0,\n"+
   "\"speed\": \"fast\",\n"+
   "\"err\": {}\n"+
   "},\n"+
   

  "{\n"+
  "\"title\": \"Test3\",\n"+
  "\"fullTitle\": \"Test3-FT\",\n"+
  "\"file\": null,\n"+
  "\"duration\": 0,\n"+
  "\"currentRetry\": 0,\n"+
  "\"speed\": \"fast\",\n"+
  "\"err\": {}\n"+
 "}\n"+

"]\n"+
"}\n";
var rez2= "{\n"+
"\"stats\": {\n" +
"\"suites\": 2,\n"+
"\"tests\": 3,\n"+
"\"passes\": 1,\n"+
"\"pending\": 0,\n"+
"\"failures\": 2,\n"+
"\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
"\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
"\"duration\": 9\n"+
"},\n"+

"\"tests\": [\n"+
"{\n"+
    "\"title\": \"Test1\",\n"+
    "\"fullTitle\": \"Test1-FT\",\n"+
    "\"file\": null,\n"+
    "\"duration\": 1,\n"+
    "\"currentRetry\": 0,\n"+
    "\"speed\": \"fast\",\n"+
    "\"err\": {}\n"+
  "},\n"+

  "{\n"+
  "\"title\": \"Test2\",\n"+
  "\"fullTitle\": \"Test2-FT\",\n"+
  "\"file\": null,\n"+
  "\"duration\": 1,\n"+
  "\"currentRetry\": 0,\n"+
  "\"speed\": \"fast\",\n"+
  "\"err\": {}\n"+
"},\n"+
          
"{\n"+
    "\"title\": \"Test3\",\n"+
    "\"fullTitle\": \"Test3-FT\",\n"+
    "\"file\": null,\n"+
    "\"duration\": 0,\n"+
    "\"currentRetry\": 0,\n"+
    "\"speed\": \"fast\",\n"+
    "\"err\": {}\n"+
  "}\n"+

"],\n"+

"\"pending\": [],\n"+
"\"failures\": [\n"+
"{\n"+
"\"title\": \"Test1\",\n"+
"\"fullTitle\": \"Test1-FT\",\n"+
"\"file\": null,\n"+
"\"duration\": 1,\n"+
"\"currentRetry\": 0,\n"+
"\"speed\": \"fast\",\n"+
"\"err\": {}\n"+
"},\n"+

"{\n"+
"\"title\": \"Test3\",\n"+
"\"fullTitle\": \"Test3-FT\",\n"+
"\"file\": null,\n"+
"\"duration\": 0,\n"+
"\"currentRetry\": 0,\n"+
"\"speed\": \"fast\",\n"+
"\"err\": {}\n"+
"}\n"+

"],\n"+

"\"passes\": [\n"+

"{\n"+
"\"title\": \"Test2\",\n"+
"\"fullTitle\": \"Test2-FT\",\n"+
"\"file\": null,\n"+
"\"duration\": 0,\n"+
"\"currentRetry\": 0,\n"+
"\"speed\": \"fast\",\n"+
"\"err\": {}\n"+
"}\n"+
"]\n"+
"}\n";
     const test=TestoviParser.porediRezultate(rez1,rez2);

     assert.equal(JSON.stringify(test),"{\"promjena\":\"75%\",\"greske\":[\"Test4-FT\",\"Test1-FT\",\"Test3-FT\"]}");
   });
   it('Potpuno razliciti testovi', function() {
    var rez1="{\n"+
    "\"stats\": {\n" +
    "\"suites\": 2,\n"+
    "\"tests\": 3,\n"+
    "\"passes\": 1,\n"+
    "\"pending\": 0,\n"+
    "\"failures\": 2,\n"+
    "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
    "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
    "\"duration\": 9\n"+
  "},\n"+

  "\"tests\": [\n"+
    "{\n"+
        "\"title\": \"ATest1\",\n"+
        "\"fullTitle\": \"ATest1-FT\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 1,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
      "},\n"+

      "{\n"+
      "\"title\": \"Test4\",\n"+
      "\"fullTitle\": \"Test4-FT\",\n"+
      "\"file\": null,\n"+
      "\"duration\": 0,\n"+
      "\"currentRetry\": 0,\n"+
      "\"speed\": \"fast\",\n"+
      "\"err\": {}\n"+
    "},\n"+
   
              
    "{\n"+
        "\"title\": \"BTest3\",\n"+
        "\"fullTitle\": \"BTest3-FT\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
      "}\n"+
    
   "],\n"+
    
   "\"pending\": [],\n"+
   "\"failures\": [\n"+
   "{\n"+
   "\"title\": \"ATest1\",\n"+
   "\"fullTitle\": \"ATest1-FT\",\n"+
   "\"file\": null,\n"+
   "\"duration\": 1,\n"+
   "\"currentRetry\": 0,\n"+
   "\"speed\": \"fast\",\n"+
   "\"err\": {}\n"+
 "},\n"+

   "{\n"+
   "\"title\": \"Test4\",\n"+
   "\"fullTitle\": \"Test4-FT\",\n"+
   "\"file\": null,\n"+
   "\"duration\": 0,\n"+
   "\"currentRetry\": 0,\n"+
   "\"speed\": \"fast\",\n"+
   "\"err\": {}\n"+
 "}\n"+
   "],\n"+
   "\"passes\": [\n"+   

  "{\n"+
  "\"title\": \"BTest3\",\n"+
  "\"fullTitle\": \"BTest3-FT\",\n"+
  "\"file\": null,\n"+
  "\"duration\": 0,\n"+
  "\"currentRetry\": 0,\n"+
  "\"speed\": \"fast\",\n"+
  "\"err\": {}\n"+
 "}\n"+

"]\n"+
"}\n";
var rez2= "{\n"+
"\"stats\": {\n" +
"\"suites\": 2,\n"+
"\"tests\": 3,\n"+
"\"passes\": 1,\n"+
"\"pending\": 0,\n"+
"\"failures\": 2,\n"+
"\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
"\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
"\"duration\": 9\n"+
"},\n"+

"\"tests\": [\n"+
"{\n"+
    "\"title\": \"Test1\",\n"+
    "\"fullTitle\": \"Test1-FT\",\n"+
    "\"file\": null,\n"+
    "\"duration\": 1,\n"+
    "\"currentRetry\": 0,\n"+
    "\"speed\": \"fast\",\n"+
    "\"err\": {}\n"+
  "},\n"+

  "{\n"+
  "\"title\": \"Test2\",\n"+
  "\"fullTitle\": \"Test2-FT\",\n"+
  "\"file\": null,\n"+
  "\"duration\": 1,\n"+
  "\"currentRetry\": 0,\n"+
  "\"speed\": \"fast\",\n"+
  "\"err\": {}\n"+
"},\n"+
          
"{\n"+
    "\"title\": \"Test3\",\n"+
    "\"fullTitle\": \"Test3-FT\",\n"+
    "\"file\": null,\n"+
    "\"duration\": 0,\n"+
    "\"currentRetry\": 0,\n"+
    "\"speed\": \"fast\",\n"+
    "\"err\": {}\n"+
  "}\n"+

"],\n"+

"\"pending\": [],\n"+
"\"failures\": [\n"+
"{\n"+
"\"title\": \"Test1\",\n"+
"\"fullTitle\": \"Test1-FT\",\n"+
"\"file\": null,\n"+
"\"duration\": 1,\n"+
"\"currentRetry\": 0,\n"+
"\"speed\": \"fast\",\n"+
"\"err\": {}\n"+
"},\n"+

"{\n"+
"\"title\": \"Test3\",\n"+
"\"fullTitle\": \"Test3-FT\",\n"+
"\"file\": null,\n"+
"\"duration\": 0,\n"+
"\"currentRetry\": 0,\n"+
"\"speed\": \"fast\",\n"+
"\"err\": {}\n"+
"}\n"+

"],\n"+

"\"passes\": [\n"+

"{\n"+
"\"title\": \"Test2\",\n"+
"\"fullTitle\": \"Test2-FT\",\n"+
"\"file\": null,\n"+
"\"duration\": 0,\n"+
"\"currentRetry\": 0,\n"+
"\"speed\": \"fast\",\n"+
"\"err\": {}\n"+
"}\n"+
"]\n"+
"}\n";
     const test=TestoviParser.porediRezultate(rez1,rez2);

     assert.equal(JSON.stringify(test),"{\"promjena\":\"80%\",\"greske\":[\"ATest1-FT\",\"Test4-FT\",\"Test1-FT\",\"Test3-FT\"]}");
   });  
   it('Neispravan format jsona', function() {
   
     const test=TestoviParser.porediRezultate("rez1","rez2");

     assert.equal(JSON.stringify(test),"{\"promjena\":\"0%\",\"greske\":[\"Testovi se ne mogu izvršiti\"]}");
   });
   it('Provjera sortiranja gresaka', function() {
    var rez1="{\n"+
    "\"stats\": {\n" +
    "\"suites\": 2,\n"+
    "\"tests\": 3,\n"+
    "\"passes\": 1,\n"+
    "\"pending\": 0,\n"+
    "\"failures\": 2,\n"+
    "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
    "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
    "\"duration\": 9\n"+
  "},\n"+

  "\"tests\": [\n"+
    "{\n"+
        "\"title\": \"z\",\n"+
        "\"fullTitle\": \"z\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 1,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
      "},\n"+

      "{\n"+
      "\"title\": \"u\",\n"+
      "\"fullTitle\": \"u\",\n"+
      "\"file\": null,\n"+
      "\"duration\": 0,\n"+
      "\"currentRetry\": 0,\n"+
      "\"speed\": \"fast\",\n"+
      "\"err\": {}\n"+
    "},\n"+
   
              
    "{\n"+
        "\"title\": \"h\",\n"+
        "\"fullTitle\": \"h\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
      "}\n"+
    
   "],\n"+
    
   "\"pending\": [],\n"+
   "\"failures\": [\n"+
   "{\n"+
   "\"title\": \"z\",\n"+
   "\"fullTitle\": \"z\",\n"+
   "\"file\": null,\n"+
   "\"duration\": 1,\n"+
   "\"currentRetry\": 0,\n"+
   "\"speed\": \"fast\",\n"+
   "\"err\": {}\n"+
 "},\n"+

   "{\n"+
   "\"title\": \"u\",\n"+
   "\"fullTitle\": \"u\",\n"+
   "\"file\": null,\n"+
   "\"duration\": 0,\n"+
   "\"currentRetry\": 0,\n"+
   "\"speed\": \"fast\",\n"+
   "\"err\": {}\n"+
 "}\n"+
   "],\n"+
   "\"passes\": [\n"+   

  "{\n"+
  "\"title\": \"h\",\n"+
  "\"fullTitle\": \"h\",\n"+
  "\"file\": null,\n"+
  "\"duration\": 0,\n"+
  "\"currentRetry\": 0,\n"+
  "\"speed\": \"fast\",\n"+
  "\"err\": {}\n"+
 "}\n"+

"]\n"+
"}\n";
var rez2= "{\n"+
"\"stats\": {\n" +
"\"suites\": 2,\n"+
"\"tests\": 3,\n"+
"\"passes\": 1,\n"+
"\"pending\": 0,\n"+
"\"failures\": 2,\n"+
"\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
"\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
"\"duration\": 9\n"+
"},\n"+

"\"tests\": [\n"+
"{\n"+
    "\"title\": \"a\",\n"+
    "\"fullTitle\": \"a\",\n"+
    "\"file\": null,\n"+
    "\"duration\": 1,\n"+
    "\"currentRetry\": 0,\n"+
    "\"speed\": \"fast\",\n"+
    "\"err\": {}\n"+
  "},\n"+

  "{\n"+
  "\"title\": \"b\",\n"+
  "\"fullTitle\": \"b\",\n"+
  "\"file\": null,\n"+
  "\"duration\": 1,\n"+
  "\"currentRetry\": 0,\n"+
  "\"speed\": \"fast\",\n"+
  "\"err\": {}\n"+
"},\n"+
          
"{\n"+
    "\"title\": \"c\",\n"+
    "\"fullTitle\": \"c\",\n"+
    "\"file\": null,\n"+
    "\"duration\": 0,\n"+
    "\"currentRetry\": 0,\n"+
    "\"speed\": \"fast\",\n"+
    "\"err\": {}\n"+
  "}\n"+

"],\n"+

"\"pending\": [],\n"+
"\"failures\": [\n"+
"{\n"+
"\"title\": \"a\",\n"+
"\"fullTitle\": \"a\",\n"+
"\"file\": null,\n"+
"\"duration\": 1,\n"+
"\"currentRetry\": 0,\n"+
"\"speed\": \"fast\",\n"+
"\"err\": {}\n"+
"},\n"+

"{\n"+
"\"title\": \"c\",\n"+
"\"fullTitle\": \"c\",\n"+
"\"file\": null,\n"+
"\"duration\": 0,\n"+
"\"currentRetry\": 0,\n"+
"\"speed\": \"fast\",\n"+
"\"err\": {}\n"+
"}\n"+

"],\n"+

"\"passes\": [\n"+

"{\n"+
"\"title\": \"b\",\n"+
"\"fullTitle\": \"b\",\n"+
"\"file\": null,\n"+
"\"duration\": 0,\n"+
"\"currentRetry\": 0,\n"+
"\"speed\": \"fast\",\n"+
"\"err\": {}\n"+
"}\n"+
"]\n"+
"}\n";
     const test=TestoviParser.porediRezultate(rez1,rez2);

     assert.equal(JSON.stringify(test),"{\"promjena\":\"80%\",\"greske\":[\"u\",\"z\",\"a\",\"c\"]}");
   });
 });
});
