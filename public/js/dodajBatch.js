window.onload = function () {
  document.getElementById('ucitaj').addEventListener('click', () => {
    var csv = document.getElementById("csv").value;

    StudentAjax.dodajBatch(csv, (err, data) => {
      if (err != null) {
        document.getElementById("ajaxstatus").textContent = (JSON.stringify(err));
      } else {
        document.getElementById("ajaxstatus").textContent = (JSON.stringify(data));
      }
    });

  });
}