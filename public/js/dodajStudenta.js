window.onload = function () {
  document.getElementById('ucitaj').addEventListener('click', () => {
    var ime = document.getElementsByName("ime")[0].value;
    var prezime = document.getElementsByName("prezime")[0].value;
    var index = document.getElementsByName("index")[0].value;
    var grupa = document.getElementsByName("grupa")[0].value;
    let objekat = { ime, prezime, index, grupa };
    StudentAjax.dodajStudenta(objekat, (err, data) => {
      if (err != null) {
        document.getElementById("ajaxstatus").textContent = (JSON.stringify(err));
      } else {
        document.getElementById("ajaxstatus").textContent = (JSON.stringify(data));
      }
    });

  });
}