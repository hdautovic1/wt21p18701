
chai.should();
let assert = chai.assert;

describe('VjezbeAjax', function () {
  beforeEach(function () {
    this.xhr = sinon.useFakeXMLHttpRequest();
    this.requests = [];
    this.xhr.onCreate = function (xhr) {
      this.requests.push(xhr);
    }.bind(this);
  });
  afterEach(function () {
    this.xhr.restore();
  });

  it('dohvatiPodatke', function (done) {
    let data = { brojVjezbi: 4, brojZadataka: [3, 4, 4, 2] };
    let dataJson = JSON.stringify(data);

    VjezbeAjax.dohvatiPodatke(function (err, result) {
      data.should.deep.equal(result);
      done();
    });

    this.requests[0].respond(200, { 'Content-Type': 'text/json' }, dataJson);
  });


  it('posaljiPodatke', function () {
    let data = { brojVjezbi: 4, brojZadataka: [1, 2, 3, 4] };
    let dataJson = JSON.stringify(data);

    VjezbeAjax.posaljiPodatke(data, function () { });

    this.requests[0].requestBody.should.equal(dataJson);
  });

  it('dodajInputPolja', function () {
    let dom = document.getElementById('input');
    let broj = 2;
    VjezbeAjax.dodajInputPolja(dom, broj);
    for (let i = 0; i < broj; i++) {
      assert.isNotNull(document.getElementById('z' + i));
    }
  });
  it('iscrtajVjezbe', function () {
    let dom = document.getElementById('iscrtajVjezbe');
    let objekat = { brojVjezbi: 5, brojZadataka: [1, 2, 3, 4, 5] };
    VjezbeAjax.iscrtajVjezbe(dom, objekat);
    for (let i = 1; i < objekat.brojVjezbi; i++) {
      assert.isNotNull(document.getElementById('vjezba' + i + 'btn'));
    }

  });
  it('iscrtajZadatke', function () {
    let dom = document.getElementById('zadaciTest');
    let broj = 5;
    VjezbeAjax.iscrtajZadatke(dom, broj);
    for (let i = 1; i < broj; i++) {
      assert.isNotNull(document.getElementById('zadatak' + i));
    }
  });

});
