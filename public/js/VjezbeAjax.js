var prikazani = [];
var prethodniDOM = null;

let VjezbeAjax = (function () {
  const dodajInputPolja = function (DOMelementDIVauFormi, brojVjezbi) {
    let html = "";
    for (let i = 0; i < brojVjezbi; i++) {
      let br = i + 1;
      html += "<label>Broj zadataka vježbe " + br + ": </label><input type=\"number\" id=\"z" + i + "\" name=\"z" + i + "\" value=\"4\">";
    }
    html += "<input type=\"button\" onClick=\"posalji()\" value=\"Pošalji\" id=\"posalji\"><br>"
    DOMelementDIVauFormi.innerHTML = html;
    return;
  }
  const posaljiPodatke = function (vjezbeObjekat, callbackFja) {
    var ajax = new XMLHttpRequest();

    ajax.onreadystatechange = function () {
      if (ajax.readyState == 4 && ajax.status == 200) {
        var jsonRez = JSON.parse(ajax.responseText);
        callbackFja(null, jsonRez);
      } else if (ajax.readyState == 4 && ajax.status == 400) {
        var jsonRez = JSON.parse(ajax.responseText);
        callbackFja(jsonRez.data, null);
      }
    }
    ajax.open("POST", "http://localhost:3000/vjezbe", true);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send(JSON.stringify(vjezbeObjekat));
    return;
  }
  const dohvatiPodatke = function (callbackFja) {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {
      if (ajax.readyState == 4 && ajax.status == 200)
        callbackFja(null, JSON.parse(ajax.responseText));
      if (ajax.readyState == 4 && ajax.status == 400)
        callbackFja(JSON.parse(ajax.responseText).data, null);
    }
    ajax.open("GET", "/vjezbe", true);
    ajax.send();
    return;
  }
  const iscrtajVjezbe = function (divDOMelement, objekat) {
    let html = "";
    for (let i = 0; i < objekat.brojVjezbi; i++) {
      prikazani[i] = false;
      let br = i + 1;
      html += "<div id=\"divVjezba" + br + "\">";
      html += "<input type=\"button\" id=\"vjezba" + br + "btn\" class=\"vjezba\" onClick=\"prikazi(" + br + ")\" value=\"Vježba "
        + br + "\">";

      html += "</div><div class=\"zadaci\" style=\"display:none\" value =\"" + objekat.brojZadataka[i] + "\" id=\"zadaci" + br + "\" ></div>";
    }
    divDOMelement.innerHTML = html;
    return;
  }
  const iscrtajZadatke = function (divDOMelement, brojZadataka) {
    let str = divDOMelement.getAttribute("id");
    let broj = str.slice(-1);

    if (prethodniDOM != null && prethodniDOM != divDOMelement) {
      prethodniDOM.style.display = "none";
      prethodniDOM = divDOMelement;
    } else {
      prethodniDOM = divDOMelement;
    }

    if (prikazani[broj - 1] == true) {
      if (divDOMelement.style.display === "none") {
        divDOMelement.style.display = "grid";
      } else {
        divDOMelement.style.display = "none";
      }
    } else {
      prikazani[broj - 1] = true;
      divDOMelement.style.display = "grid";
      let html = "";
      for (let i = 0; i < brojZadataka; i++) {
        let br = i + 1;
        html += "<input type=\"button\"  class=\"zadatak\" value=\"ZADATAK " + br + "\" id=\"zadatak" + br + "\"> ";
      }
      divDOMelement.innerHTML = html;
    }

    return;
  }
  return {
    dodajInputPolja: dodajInputPolja,
    posaljiPodatke: posaljiPodatke,
    dohvatiPodatke: dohvatiPodatke,
    iscrtajVjezbe: iscrtajVjezbe,
    iscrtajZadatke: iscrtajZadatke
  }
}());