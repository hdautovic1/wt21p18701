window.onload = function () {
  document.getElementById('postavi').addEventListener('click', () => {

    var index = document.getElementsByName("index")[0].value;
    var grupa = document.getElementsByName("grupa")[0].value;
    StudentAjax.postaviGrupu(index, grupa, (err, data) => {
      if (err != null) {
        document.getElementById("ajaxstatus").textContent = (JSON.stringify(err));
      } else {
        document.getElementById("ajaxstatus").textContent = (JSON.stringify(data));
      }
    });

  });
}