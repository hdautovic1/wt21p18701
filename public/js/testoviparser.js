let TestoviParser = (function () {
  const dajTacnost = function (stringJSON) {
    var obj,o,greske=[];    
    try{
      obj = JSON.parse(stringJSON);
      var i=0;
      if(obj.stats.failures>0){
        for(i=0;i<obj.stats.failures;i++){
            greske.push(obj.failures[i].fullTitle);
        }
      }
      greske.sort(function(a, b){
        if(a < b) { return -1; }
        if(a > b) { return 1; }
        return 0;
    })
      const tacnost=obj.stats.passes/obj.stats.tests*100;
      o = {tacnost: Math.round(tacnost*10)/10 +"%" , greske};  
    }catch(e){     
      greske.push("Testovi se ne mogu izvršiti");
      o = {tacnost: "0%" , greske};   
    }      
     return o;      
  }
  
  const porediRezultate = function (rezultat1,rezultat2) {
    var obj1,obj2,i,j,identicni=true,o;
    try{
      obj1=JSON.parse(rezultat1);
      obj2=JSON.parse(rezultat2);  
    
      if(obj1.stats.tests!=obj2.stats.tests){
      identicni=false;
      }else{

       for(i=0;i<obj1.stats.tests;i++){
       var pronadjen=false;
       for(j=0;j<obj2.stats.tests;j++){
         if(obj1.tests[i].fullTitle==obj2.tests[j].fullTitle){
           pronadjen=true;
           break;
         }
       }
       if(pronadjen==false){
         identicni=false;
         break;
       }
     }
    }

    if(identicni==true){
      var greske=[];
      var j=0;
      
      if(obj2.stats.failures>0){
        for(j=0;j<obj2.stats.failures;j++){
            greske.push(obj2.failures[j].fullTitle);
        }
      }
      greske.sort(function(a, b){
        if(a < b) { return -1; }
        if(a > b) { return 1; }
        return 0;
    });
      const tacnost=obj2.stats.passes/obj2.stats.tests*100;
      o = {promjena: Math.round(tacnost*10)/10 +"%" , greske};  
    
    }else{
     
      var greske=[];
      var br=0;
      for(i=0;i<obj1.stats.failures;i++){
        var title = obj1.failures[i].fullTitle;
        var pronadjen=false;
        for(j=0;j<obj2.stats.failures;j++){
          if(obj2.failures[j].fullTitle==title){
            pronadjen=true;
            break;
          }
        }
        if(pronadjen==false){
          br++;
          greske.push(title);
        }
      }
      greske.sort(function(a, b){
        if(a < b) { return -1; }
        if(a > b) { return 1; }
        return 0;
    });
  var greske2=[];
    for(j=0;j<obj2.stats.failures;j++){
     greske2.push(obj2.failures[j].fullTitle)     
    }

    greske2.sort(function(a, b){
      if(a < b) { return -1; }
      if(a > b) { return 1; }
      return 0;
  });
  for(j=0;j<obj2.stats.failures;j++){
    greske.push(greske2[j]) ;    
   }

      const tacnost=((br+obj2.stats.failures)/(br+obj2.stats.tests))*100;
      o = {promjena: Math.round(tacnost*10)/10 +"%" , greske}; 

    }
   
    }catch(e){   
      var greske=[];  
     greske.push("Testovi se ne mogu izvršiti");
      o = {promjena: "0%" , greske};   
    } 
    return o; 
  } 

  return {
    dajTacnost: dajTacnost,
    porediRezultate: porediRezultate    
  }
}());