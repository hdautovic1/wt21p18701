
let StudentAjax = (function () {

  const dodajStudenta = function (studentObjekat, callbackFja) {
    var ajax = new XMLHttpRequest();

    ajax.onreadystatechange = function () {
      if (ajax.readyState == 4 && ajax.status == 200) {
        var jsonRez = JSON.parse(ajax.responseText);
        callbackFja(null, jsonRez);
      } else if (ajax.readyState == 4 && ajax.status == 400) {
        var jsonRez = JSON.parse(ajax.responseText);
        callbackFja(jsonRez, null);
      }
    }
    ajax.open("POST", "http://localhost:3000/student", true);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send(JSON.stringify(studentObjekat));
    return;
  }
  const postaviGrupu = function (index, grupa, callbackFja) {
    var ajax = new XMLHttpRequest();

    ajax.onreadystatechange = function () {
      if (ajax.readyState == 4 && ajax.status == 200) {
        var jsonRez = JSON.parse(ajax.responseText);
        callbackFja(null, jsonRez);
      } else if (ajax.readyState == 4 && ajax.status == 400) {
        var jsonRez = JSON.parse(ajax.responseText);
        callbackFja(jsonRez, null);
      }
    }
    let objekat = { grupa: grupa };
    ajax.open("PUT", "http://localhost:3000/student/" + index, true);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send(JSON.stringify(objekat));
    return;
  }
  const dodajBatch = function (csv, callbackFja) {
    var ajax = new XMLHttpRequest();

    ajax.onreadystatechange = function () {
      if (ajax.readyState == 4 && ajax.status == 200) {
        var jsonRez = JSON.parse(ajax.responseText);
        callbackFja(null, jsonRez);
      } else if (ajax.readyState == 4 && ajax.status == 400) {
        var jsonRez = JSON.parse(ajax.responseText);
        callbackFja(jsonRez, null);
      }
    }
    ajax.open("POST", "http://localhost:3000/batch/student", true);
    ajax.send(csv);
    return;
  }

  return {
    dodajStudenta: dodajStudenta,
    postaviGrupu: postaviGrupu,
    dodajBatch: dodajBatch
  }
}());