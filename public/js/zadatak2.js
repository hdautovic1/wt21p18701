let assert = chai.assert;
describe('TestoviParser', function() {
 describe('dajTacnost()', function() {
   it('Prolaze svi', function() {
     const test=TestoviParser.dajTacnost(
       "{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 3,\n"+
          "\"passes\": 3,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 0,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"Test1\",\n"+
              "\"fullTitle\": \"Test1-FT\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"Test2\",\n"+
              "\"fullTitle\": \"Test2-FT\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+
                    
          "{\n"+
              "\"title\": \"Test3\",\n"+
              "\"fullTitle\": \"Test3-FT\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [],\n"+
         "\"passes\": [\n"+
         "{\n"+
         "\"title\": \"Test1\",\n"+
         "\"fullTitle\": \"Test1-FT\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "},\n"+
         
         "{\n"+
         "\"title\": \"Test2\",\n"+
         "\"fullTitle\": \"Test2-FT\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 0,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
        "},\n"+

        "{\n"+
        "\"title\": \"Test3\",\n"+
        "\"fullTitle\": \"Test3-FT\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
       "}\n"+

      "]\n"+
    "}\n"
    );

     assert.equal(JSON.stringify(test),"{\"tacnost\":\"100%\",\"greske\":[]}");
   });   
   it('Pada jedan', function() {
    const test=TestoviParser.dajTacnost(
      "{\n"+
      "\"stats\": {\n" +
      "\"suites\": 2,\n"+
      "\"tests\": 3,\n"+
      "\"passes\": 2,\n"+
      "\"pending\": 0,\n"+
      "\"failures\": 1,\n"+
      "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
      "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
      "\"duration\": 9\n"+
    "},\n"+

    "\"tests\": [\n"+
      "{\n"+
          "\"title\": \"Test1\",\n"+
          "\"fullTitle\": \"Test1-FT\",\n"+
          "\"file\": null,\n"+
          "\"duration\": 1,\n"+
          "\"currentRetry\": 0,\n"+
          "\"speed\": \"fast\",\n"+
          "\"err\": {}\n"+
        "},\n"+
                
      "{\n"+
          "\"title\": \"Test2\",\n"+
          "\"fullTitle\": \"Test2-FT\",\n"+
          "\"file\": null,\n"+
          "\"duration\": 0,\n"+
          "\"currentRetry\": 0,\n"+
          "\"speed\": \"fast\",\n"+
          "\"err\": {}\n"+
        "},\n"+

        "{\n"+
          "\"title\": \"Test3\",\n"+
          "\"fullTitle\": \"Test3-FT\",\n"+
          "\"file\": null,\n"+
          "\"duration\": 0,\n"+
          "\"currentRetry\": 0,\n"+
          "\"speed\": \"fast\",\n"+
          "\"err\": {}\n"+
        "}\n"+
      
     "],\n"+
      
     "\"pending\": [],\n"+
     "\"failures\": [\n"+
     "{\n"+
     "\"title\": \"Test3\",\n"+
     "\"fullTitle\": \"Test3-FT\",\n"+
     "\"file\": null,\n"+
     "\"duration\": 1,\n"+
     "\"currentRetry\": 0,\n"+
     "\"speed\": \"fast\",\n"+
     "\"err\": {}\n"+
     "}\n"+
    "],\n"+

     "\"passes\": [\n"+
     "{\n"+
     "\"title\": \"Test1\",\n"+
     "\"fullTitle\": \"Test1-FT\",\n"+
     "\"file\": null,\n"+
     "\"duration\": 1,\n"+
     "\"currentRetry\": 0,\n"+
     "\"speed\": \"fast\",\n"+
     "\"err\": {}\n"+
     "},\n"+
     
     "{\n"+
     "\"title\": \"Test2\",\n"+
     "\"fullTitle\": \"Test2-FT\",\n"+
     "\"file\": null,\n"+
     "\"duration\": 0,\n"+
     "\"currentRetry\": 0,\n"+
     "\"speed\": \"fast\",\n"+
     "\"err\": {}\n"+
    "}\n"+
  "]\n"+
"}\n"
      );

    assert.equal(JSON.stringify(test),"{\"tacnost\":\"66.7%\",\"greske\":[\"Test3-FT\"]}");
  }); 
  it('Prolazi jedan', function() {
    const test=TestoviParser.dajTacnost(
      "{\n"+
      "\"stats\": {\n" +
      "\"suites\": 2,\n"+
      "\"tests\": 3,\n"+
      "\"passes\": 1,\n"+
      "\"pending\": 0,\n"+
      "\"failures\": 2,\n"+
      "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
      "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
      "\"duration\": 9\n"+
    "},\n"+

    "\"tests\": [\n"+
      "{\n"+
          "\"title\": \"Test1\",\n"+
          "\"fullTitle\": \"Test1-FT\",\n"+
          "\"file\": null,\n"+
          "\"duration\": 1,\n"+
          "\"currentRetry\": 0,\n"+
          "\"speed\": \"fast\",\n"+
          "\"err\": {}\n"+
        "},\n"+

        "{\n"+
        "\"title\": \"Test2\",\n"+
        "\"fullTitle\": \"Test2-FT\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 1,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
      "},\n"+
                
      "{\n"+
          "\"title\": \"Test3\",\n"+
          "\"fullTitle\": \"ATest3-FT\",\n"+
          "\"file\": null,\n"+
          "\"duration\": 0,\n"+
          "\"currentRetry\": 0,\n"+
          "\"speed\": \"fast\",\n"+
          "\"err\": {}\n"+
        "}\n"+
      
     "],\n"+
      
     "\"pending\": [],\n"+
     "\"failures\": [\n"+
     "{\n"+
     "\"title\": \"Test1\",\n"+
     "\"fullTitle\": \"Test1-FT\",\n"+
     "\"file\": null,\n"+
     "\"duration\": 1,\n"+
     "\"currentRetry\": 0,\n"+
     "\"speed\": \"fast\",\n"+
     "\"err\": {}\n"+
     "},\n"+

     "{\n"+
     "\"title\": \"Test3\",\n"+
     "\"fullTitle\": \"ATest3-FT\",\n"+
     "\"file\": null,\n"+
     "\"duration\": 0,\n"+
     "\"currentRetry\": 0,\n"+
     "\"speed\": \"fast\",\n"+
     "\"err\": {}\n"+
   "}\n"+
   
     "],\n"+

     "\"passes\": [\n"+

     "{\n"+
     "\"title\": \"Test2\",\n"+
     "\"fullTitle\": \"Test2-FT\",\n"+
     "\"file\": null,\n"+
     "\"duration\": 0,\n"+
     "\"currentRetry\": 0,\n"+
     "\"speed\": \"fast\",\n"+
     "\"err\": {}\n"+
    "}\n"+
  "]\n"+
"}\n"
      );

    assert.equal(JSON.stringify(test),"{\"tacnost\":\"33.3%\",\"greske\":[\"ATest3-FT\",\"Test1-FT\"]}");
  }); 
  it('Padaju svi', function() {
    const test=TestoviParser.dajTacnost(
      "{\n"+
      "\"stats\": {\n" +
      "\"suites\": 2,\n"+
      "\"tests\": 3,\n"+
      "\"passes\": 0,\n"+
      "\"pending\": 0,\n"+
      "\"failures\": 3,\n"+
      "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
      "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
      "\"duration\": 9\n"+
    "},\n"+

    "\"tests\": [\n"+
      "{\n"+
          "\"title\": \"Test1\",\n"+
          "\"fullTitle\": \"Test1-FT\",\n"+
          "\"file\": null,\n"+
          "\"duration\": 1,\n"+
          "\"currentRetry\": 0,\n"+
          "\"speed\": \"fast\",\n"+
          "\"err\": {}\n"+
        "},\n"+

        "{\n"+
        "\"title\": \"Test2\",\n"+
        "\"fullTitle\": \"Test2-FT\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 1,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
      "},\n"+
                
      "{\n"+
          "\"title\": \"Test3\",\n"+
          "\"fullTitle\": \"ATest3-FT\",\n"+
          "\"file\": null,\n"+
          "\"duration\": 0,\n"+
          "\"currentRetry\": 0,\n"+
          "\"speed\": \"fast\",\n"+
          "\"err\": {}\n"+
        "}\n"+
      
     "],\n"+
      
     "\"pending\": [],\n"+
     "\"failures\": [\n"+
     "{\n"+
     "\"title\": \"Test1\",\n"+
     "\"fullTitle\": \"Test1-FT\",\n"+
     "\"file\": null,\n"+
     "\"duration\": 1,\n"+
     "\"currentRetry\": 0,\n"+
     "\"speed\": \"fast\",\n"+
     "\"err\": {}\n"+
     "},\n"+

     "{\n"+
     "\"title\": \"Test2\",\n"+
     "\"fullTitle\": \"Test2-FT\",\n"+
     "\"file\": null,\n"+
     "\"duration\": 1,\n"+
     "\"currentRetry\": 0,\n"+
     "\"speed\": \"fast\",\n"+
     "\"err\": {}\n"+
     "},\n"+

     "{\n"+
     "\"title\": \"Test3\",\n"+
     "\"fullTitle\": \"ATest3-FT\",\n"+
     "\"file\": null,\n"+
     "\"duration\": 0,\n"+
     "\"currentRetry\": 0,\n"+
     "\"speed\": \"fast\",\n"+
     "\"err\": {}\n"+
   "}\n"+
   
     "],\n"+

     "\"passes\": []\n"+
"}\n"
      );

    assert.equal(JSON.stringify(test),"{\"tacnost\":\"0%\",\"greske\":[\"ATest3-FT\",\"Test1-FT\",\"Test2-FT\"]}");
  }); 
  it('Netacan JSON string', function() {
    const test=TestoviParser.dajTacnost(" ");

      assert.equal(JSON.stringify(test),"{\"tacnost\":\"0%\",\"greske\":[\"Testovi se ne mogu izvršiti\"]}");
  }); 
 });
});
