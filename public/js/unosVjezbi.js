window.onload = function () {
  document.getElementById('ucitaj').addEventListener('click', () => {
    const container = document.getElementById("container");
    var broj = document.getElementsByName("broj")[0].value;
    if (broj >= 1 && broj <= 15) {
      VjezbeAjax.dodajInputPolja(container, broj);
    }

  });
}
function posalji() {
  var broj = document.getElementsByName("broj")[0].value;
  var niz = [];
  for (let i = 0; i < broj; i++) {
    if (document.getElementById("z" + i) != null)
      niz[i] = document.getElementById("z" + i).value;
  }
  var objekat = { brojVjezbi: broj, brojZadataka: niz };
  VjezbeAjax.posaljiPodatke(objekat, (err, data) => {
    if (err != null) {
      console.log(JSON.stringify(err));
    } else {
      console.log(JSON.stringify(data));
    }
  });

};